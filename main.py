from flask import Flask, render_template
import json
import csv
import pandas as pd

app = Flask(__name__)

@app.route("/")
def index():


    df = pd.read_csv('https://projects.fivethirtyeight.com/trump-approval-data/approval_polllist.csv')
    trumpian = df['president'] == 'Donald Trump'
    gallup = df['pollster'] == 'Gallup'
    filtered = df[trumpian & gallup]
    filtered_again = filtered[['enddate', 'adjusted_approve']]

    data = json.loads(filtered_again.to_json(None, 'records'))


    return render_template('data.html', data=data)